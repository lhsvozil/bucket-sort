#pragma once
#include <vector>
#include "lockableVector.h"
#include "qsort.h"
#include <thread>
#include <fstream>
#include <iostream>
#include <chrono>
#include <algorithm>
#include <assert.h>
#include <stdexcept>
#include <random>
#include "BeSort.h"

template <typename T> class buckedSortedVector
{
private:
	std::vector<std::vector<T> > buckets;
	int m_bucketsCount;
	int m_range;
	int m_min;
	int m_max;
	int m_divider;
	std::ofstream m_file;
	std::ifstream m_fin;
	std::vector<std::vector<int > > workersBuckets;
public:
	void push_back(T what){
		if (what > m_max) m_max = what;
		if (what < m_min) m_min = what;
		m_vec.push_back(what);
	}
	void load(){
		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();
		m_fin.open("C:/Users/stras_000/Documents/data.bi", std::ios::in | std::ios::binary);
		int i = 0;
		T in;
		while (m_fin.read(reinterpret_cast<char *>(&in), sizeof(in))){
			i++;
			push_back(in);
		}
		m_fin.close();
		m_range = m_max;// -m_min
		m_divider = m_range / (m_bucketsCount);
		m_divider++;
		std::vector<T> oneBucket;
		for (int i = 0; i<8; i++){
			workersBuckets.push_back(oneBucket);
			workersBuckets.at(i).resize(m_range/m_bucketsCount+100);
			std::fill(workersBuckets.at(i).begin(), workersBuckets.at(i).end(), 0);
		}
		
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> dur = end - start;


		std::cout << std::endl << "Load from file " << dur.count() << "s! " << std::endl;
	}
		
	void write(){
		m_file.open("C:/Users/stras_000/Documents/result.txt");
		for (auto val : m_vec){
			m_file << val << std::endl;
		}
		m_file.close();
	}
	void write(std::vector<T> &vec){
		m_file.open("C:/Users/stras_000/Documents/custom.txt");
		for (auto val : vec){
			m_file << val << std::endl;
		}
		m_file.close();
	}
	void writeRandoms(){
		m_file.open("C:/Users/stras_000/Documents/data.bi", std::ios::out | std::ios::binary);
		std::default_random_engine generator;
		std::uniform_int_distribution<int> distribution(0, 2000000);

		for (int i = 0; i < 15000000; i++){
			m_vec.push_back(distribution(generator));
		}
		
		for (auto val : m_vec){
			m_file.write(reinterpret_cast<const char *>(&val),sizeof(val));
		}
		m_file.close();
		m_vec.resize(0);
	}
	std::vector<T> m_vec;

	void sort(){
		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();

		sortingWorkerFunc(1, 0);

		end = std::chrono::system_clock::now();
		std::chrono::duration<double> dur1 = end - start;
		std::cout << std::endl << "bucketing ( "<<m_bucketsCount<< " buckets)  " << dur1.count() << "s " << std::endl;

		int threads =1;
		std::vector<std::thread> workers;
		for (int i = 0; i < threads; i++){
			workers.push_back(std::thread(&buckedSortedVector<T>::bucketSortingWorkerFunc, this, threads, i));
		}

		//bucketSortingWorkerFunc(1, 0);
		for (int i = 0; i < threads; i++){
			workers.at(i).join();
			//std::cout << "done" << i << std::endl;
		}

		// copy buckety k sobe
		auto lastPos = m_vec.begin();
		for (int i = 0; i < m_bucketsCount; i++){
			lastPos = std::copy(buckets.at(i).begin(), buckets.at(i).end(), lastPos);
		}
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> dur = end - start;
		
		std::cout << std::endl << "bucketing + sorting " << dur.count() << "s " << std::endl;
	}


	void stdsort(){
		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();

		std::sort(m_vec.begin(), m_vec.end());

		end = std::chrono::system_clock::now();
		std::chrono::duration<double> dur = end - start;
		std::cout << std::endl << "Std::sort took " << dur.count() << "s! " << std::endl;
	}

	void mujqsortTest(){
		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();


		mujqsort<T> mq(m_vec);

		//mq.setVec(m_vec);
		//mq.sortFunc(0,m_vec.size()-1);
		//m_vec = mq.sortFunc0(m_vec);
		mq.sortFuncSw(m_vec);
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> dur = end - start;
		std::cout << std::endl << "Muj quicksort " << dur.count() << "s! " << std::endl;
	}
	void InsSortTest(){
		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();

		

		for (auto i = m_vec.begin(); i != m_vec.end(); i++){

			for (auto j = i + 1; j != m_vec.end(); j++){
					if (*i > *j){
						std::swap(*i, *j);
					}
				}
			}
		
		

		end = std::chrono::system_clock::now();
		std::chrono::duration<double> dur = end - start;
		std::cout << std::endl << "Muj quicksort " << dur.count() << "s! " << std::endl;
	}
	void BeSortTest(){
		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();

		std::cout << m_vec.size();
		BeSort bs(m_vec);

		//mq.setVec(m_vec);
		//mq.sortFunc(0,m_vec.size()-1);
		//m_vec = mq.sortFunc0(m_vec);
		bs.mergeSort();
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> dur = end - start;
		std::cout << std::endl << "Jeho sort " << dur.count() << "s! " << std::endl;
	}

	void bucketSortingWorkerFunc(int howManyWorkers, int whichWorkerAmI){
		//assert(whichWorkerAmI < 10);
		int howMuchIShouldDo = m_bucketsCount / howManyWorkers;
		//int bucketMin = m_divider*
		//mujqsort<T> mq;

		 
			
			for (int i = howMuchIShouldDo*whichWorkerAmI; i < howMuchIShouldDo*(whichWorkerAmI + 1); i++){
				
				//qsort
				/*if (buckets.at(i).size()>1){
					buckets.at(i) = mujqsort<T>::sortFunc0(buckets.at(i));
				}*/
				//insSort
				//insSort(buckets.at(i));
				BeSort bs(buckets.at(i));
				//std::sort(buckets.at(i).begin(), buckets.at(i).end());
			}
			
		/*catch (std::out_of_range){
			std::cout << buckets.at(i).at(j) - m_divider*i << std::endl << workersBuckets.at(whichWorkerAmI).size()<<std::endl;
		}*/

	}

	void sortingWorkerFunc(int howManyWorkers, int whichWorkerAmI){
		int howMuchIShouldDo = m_vec.size() / howManyWorkers;

		for (size_t i = howMuchIShouldDo*whichWorkerAmI; i < howMuchIShouldDo*(whichWorkerAmI + 1); i++){
			int whichBucket = (m_vec.at(i) / m_divider);
			//std::lock_guard<std::mutex> lock(buckets.at(whichBucket).m_mutex);
			//EnterCriticalSection(&buckets.at(whichBucket).m_mutex);
		//	buckets.at(whichBucket).push_back(m_vec.at(i));

			buckets.at(whichBucket).push_back(m_vec.at(i));
/*
			if (buckets.at(whichBucket).empty()){
				buckets.at(whichBucket).push_back(m_vec.at(i));
			}
			else{
				bool posledni = true;
				for (int j = 0; j< buckets.at(whichBucket).size(); j++){
					if (m_vec.at(i) < buckets.at(whichBucket).at(j)){
						buckets.at(whichBucket).insert(buckets.at(whichBucket).begin() + j, m_vec.at(i));
						posledni = false;
						break;
					}
				}
				if (posledni) buckets.at(whichBucket).push_back(m_vec.at(i));
			}*/
			
			//LeaveCriticalSection(&buckets.at(whichBucket).m_mutex);
		}
	};
	void insSort(std::vector<T> &vec){

		for (auto i = vec.begin(); i != vec.end(); i++){

			for (auto j = i+1; j != vec.end(); j++){
				if (*i > *j){
					std::swap(*i, *j);
				}
			}
		}
	}
	
	
	buckedSortedVector(void){
		m_min = 9999999;
		m_max = 0;
		m_bucketsCount = 550000;
		int howMuchReserve = m_vec.size() / m_bucketsCount;

		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();
		std::vector<T> oneBucket;
		//oneBucket.reserve(150);
		for (int i = 0; i<m_bucketsCount; i++){
			buckets.push_back(oneBucket);
			buckets.at(i).reserve(90);
		}
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> dur = end - start;
		std::cout << std::endl << "Loaded in " << dur.count() << "s.. " << std::endl;
	};
	buckedSortedVector(int grr){
		if (grr == 1){
			m_min = 9999999;
			m_max = 0;
			m_bucketsCount = 150000;
			int howMuchReserve = m_vec.size() / m_bucketsCount;

			std::chrono::time_point<std::chrono::system_clock> start, end;
			start = std::chrono::system_clock::now();
			std::vector<T> oneBucket;
			//oneBucket.reserve(150);
			for (int i = 0; i < m_bucketsCount; i++){
				buckets.push_back(oneBucket);
				buckets.at(i).reserve(90);
			}
			end = std::chrono::system_clock::now();
			std::chrono::duration<double> dur = end - start;
			std::cout << std::endl << "Loaded in " << dur.count() << "s.. " << std::endl;
		}
	};
	~buckedSortedVector(void){};
};

//insertion
/*
if (buckets.at(whichBucket).empty()){
	buckets.at(whichBucket).push_back(m_vec.at(i));
}
else{
	bool posledni = true;
	for (int j = 0; j< buckets.at(whichBucket).size(); j++){
		if (m_vec.at(i) < buckets.at(whichBucket).at(j)){
			buckets.at(whichBucket).insert(buckets.at(whichBucket).begin() + j, m_vec.at(i));
			posledni = false;
			break;
		}
	}
	if (posledni) buckets.at(whichBucket).push_back(m_vec.at(i));
}*/