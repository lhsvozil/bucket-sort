#pragma once
#include <vector>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <assert.h>

template <typename T> class mujqsort
{
private:
	std::vector<T> &m_vec;
	
public:
	
	mujqsort(std::vector<T> &vec):m_vec(vec){
		
	};
	void sortFunc(int begin, int end){

		//std::vector<T> left, right;
		//std::cout << vec.at(begin);
		int tmp;
		int bigger=end; //prvni vetsi nez pivot

		tmp = vec[end];
		vec[end] = vec[begin];	//pivot na konec
		vec[begin] = tmp;

		for (int i = begin; i<end;i++){
			if (vec[i]>=vec[end]){
				bigger = (bigger==end) ? i : bigger;
				bigger = (i < bigger) ? i : bigger;
			}else{
				if (bigger!=end){
					tmp = vec[i];
					vec[i] = vec[bigger];
					vec[bigger] = tmp;
					tmp = i;
					i = bigger;
					bigger = tmp;
				}
			}
		}



		tmp = vec[bigger];
		vec[bigger] = vec[end];
		vec[end] = tmp;
		bigger++;


		if ((bigger-1)-begin > 0)
			sortFunc(begin, bigger-1);

		if (end - bigger > 0) 
			sortFunc(bigger, end);

		
		
	}
	static std::vector<T> sortFunc0(std::vector<T> &vec){
		std::vector<T> left, right;
		left.reserve(vec.size());
		right.reserve(vec.size());

		for (int i = 1; i < vec.size(); i++){
			if (vec.at(0) >= vec.at(i))
				left.push_back(vec.at(i));
			else
				right.push_back(vec.at(i));
		}


		if (left.size() < 40)
			insSort(left);
		else
			left = sortFunc0(left);


		if (right.size() < 40)
			insSort(right);
		else
			right = sortFunc0(right);


		left.insert(left.end(), vec.at(0));
		left.insert(left.end(), right.begin(), right.end());
		return left;
		
	}
	void sortFuncSw(std::vector<T> &vec){
		//insSortSw(0, vec.size() - 1);
		sortFuncSwInner(0, vec.size() - 1);
	}

void sortFuncSwInner(int left, int right){
		int p = left;
		int begin = left;
		int end = right; // end tu patri
		const int kIns = 2;
		
			while (left < right){
				left++;
				if (m_vec.at(left) > m_vec.at(p)){
					while (right > left){
						if (m_vec.at(right) < m_vec.at(p))
							break;
						right--;
					}
					std::swap(m_vec.at(left), m_vec.at(right));
				}
				
				
			}
			assert(left == right);
			if (m_vec.at(left)<m_vec.at(p)){
				std::swap(m_vec.at(left), m_vec.at(p));
				if ((left - 1) - begin < kIns)
					insSortSw(begin, left - 1);
				else
					sortFuncSwInner(begin, left - 1);

				if (end - (left+1) < kIns)
					insSortSw(left + 1, end);
				else
					sortFuncSwInner(left + 1, end);

			}else{
				std::swap(m_vec.at(left - 1), m_vec.at(p));
				if ((left - 2) - begin < kIns)
					insSortSw(begin, left - 2);
				else
					sortFuncSwInner(begin, left - 2);

				if (end - (left) < kIns)
					insSortSw(left, end);
				else
					sortFuncSwInner(left, end);
			}
				/*	std::nth_element(m_vec.begin() + begin, m_vec.begin() + (end - begin) / 2, m_vec.begin() + end+1);
				std::swap(m_vec.at(begin), m_vec.at((end - begin) / 2));
				left = begin;
				right = end;
				
				*/
		
	
	/*
	if ((right - 2) - begin < 2)
		insSortSw(begin, right - 1);
	else
		sortFuncSwInner(begin, right - 2);

	if (end - right < 2)
		insSortSw(right, end);
	else
		sortFuncSwInner(right, end);


*/
}
void insSortSw(int begin, int end){
	int tmp;
	for (int i = begin; i < end; i++){

		for (int j = i + 1; j <= end; j++){
			if (m_vec.at(i) > m_vec.at(j)){
				
				std::swap(m_vec.at(i), m_vec.at(j));
			}
		}
	}
}
	static void insSort(std::vector<T> &vec){

		for (auto i = vec.begin(); i != vec.end(); i++){

			for (auto j = i + 1; j != vec.end(); j++){
				if (*i > *j){
					std::swap(*i, *j);
				}
			}
		}
	}
	/*static int* sortFuncAr(int* ar, int arSize){

		int* left =new int[arSize];
		int* right = new int[arSize];
		//int *right = (int*) malloc(sizeof(int)*arSize);
		int l = 0;
		int r = 0;

		
		for (int i = 1; i < arSize; i++){
			if (ar[0] >= ar[i]){
				left[l] = ar[i];
				++l;
			}
			else{
				right[r] = ar[i];
				++r;
			}
				
		}
		if (l > 1)
			left = sortFuncAr(left,l);

		if (r > 1)
			right = sortFuncAr(right,r);
		int tmp = ar[0];
		memcpy(ar, left, l*sizeof(int));
		ar[l] = tmp;
		memcpy(ar + (l+1)*sizeof(int), right, r*sizeof(int));
		return ar;
	}*/
	~mujqsort(){
	}
};


/*
static std::vector<T> sortFunc(std::vector<T> vec){

	std::vector<T> left, right;
	for (int i = 1; i < vec.size(); i++){
		if (vec.at(0) >= vec.at(i))
			left.push_back(vec.at(i));
		else
			right.push_back(vec.at(i));
	}
	if (left.size() > 1)
		left = sortFunc(left);

	if (right.size() > 1)
		right = sortFunc(right);

	left.insert(left.end(), vec.at(0));
	left.insert(left.end(), right.begin(), right.end());
	return left;
}*/

/**********nej*********************

static std::vector<T> sortFunc0(std::vector<T> &vec){

std::vector<T> left, right;
left.reserve(vec.size());
right.reserve(vec.size());

for (int i = 1; i < vec.size(); i++){
if (vec.at(0) >= vec.at(i))
left.push_back(vec.at(i));
else
right.push_back(vec.at(i));
}


if (left.size() < 10)
insSort(left);
else
left = sortFunc0(left);


if (right.size() < 10)
insSort(right);
else
right = sortFunc0(right);


left.insert(left.end(), vec.at(0));
left.insert(left.end(), right.begin(), right.end());
return left;

}







*/