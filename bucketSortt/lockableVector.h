#pragma once
#include <vector>
#include <mutex>
#include <Windows.h>
#include <synchapi.h>


template <typename T> class lockableVector :
	public std::vector<T>
{
public:
	//std::mutex m_mutex;
	CRITICAL_SECTION m_mutex;
	lockableVector(){};
	lockableVector(const lockableVector& other){
		//std::mutex m_mutex;
		//CRITICAL_SECTION m_mutex;
		InitializeCriticalSectionAndSpinCount(&m_mutex, 0x00000200);
	};
	~lockableVector(){};
};

