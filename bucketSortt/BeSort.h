#include <vector>
using namespace std;

class BeSort{
private:
	vector<int> &numbers;
public:
	BeSort(vector<int> &vec) :numbers(vec){
		mergeSort();
	};

void myMerge(vector<int> indexes, vector<int> merged)
{
	if (indexes.size() < 3)
	{
		return;
	}
	else
	{


		if ((indexes.size() % 2) == 0)
		{
			int left = indexes.at(0), middle = indexes.at(1), right = indexes.at(2);
			int sizeOfLeft = middle - left, sizeOfRight = right - middle, iteratorLeft = 0, iteratorRight = 0;

			while ((iteratorLeft != sizeOfLeft) && (iteratorRight != sizeOfRight))
			{
				if ((numbers.at(left + iteratorLeft)) <= (numbers.at(middle + iteratorRight)))
				{
					// merged.at(left + iteratorLeft + iteratorRight) = (numbers.at(left + iteratorLeft));
					merged.push_back(numbers.at(left + iteratorLeft));
					iteratorLeft++;
				}

				else
				{
					// merged.at(left + iteratorLeft + iteratorRight) = (numbers.at(middle + iteratorRight));
					merged.push_back(numbers.at(middle + iteratorRight));
					iteratorRight++;
				}

			}

			if (iteratorLeft == sizeOfLeft)
			{
				while (iteratorRight != sizeOfRight)
				{
					merged.push_back(numbers.at(middle + iteratorRight));
					iteratorRight++;
				}
			}

			else if (iteratorRight == sizeOfRight)
			{
				while (iteratorLeft != sizeOfLeft)
				{
					merged.push_back(numbers.at(left + iteratorLeft));
					iteratorLeft++;
				}
			}

			for (int i = merged.size(); i < numbers.size(); i++)
			{
				merged.push_back(numbers.at(i));
			}

			indexes.erase(indexes.begin() + 1);

			numbers.clear();

			numbers = merged;

			merged.clear();
		}

		for (int i = 0; i < (indexes.size() - 1); i++)
		{
			int left = indexes.at(i), middle = indexes.at(i + 1), right = indexes.at(i + 2);
			int sizeOfLeft = middle - left, sizeOfRight = right - middle, iteratorLeft = 0, iteratorRight = 0;

			while ((iteratorLeft != sizeOfLeft) && (iteratorRight != sizeOfRight))
			{
				if ((numbers.at(left + iteratorLeft)) <= (numbers.at(middle + iteratorRight)))
				{
					// merged.at(left + iteratorLeft + iteratorRight) = (numbers.at(left + iteratorLeft));
					merged.push_back(numbers.at(left + iteratorLeft));
					iteratorLeft++;
				}

				else
				{
					// merged.at(left + iteratorLeft + iteratorRight) = (numbers.at(middle + iteratorRight));
					merged.push_back(numbers.at(middle + iteratorRight));
					iteratorRight++;
				}

			}

			if (iteratorLeft == sizeOfLeft)
			{
				while (iteratorRight != sizeOfRight)
				{
					merged.push_back(numbers.at(middle + iteratorRight));
					iteratorRight++;
				}
			}

			else if (iteratorRight == sizeOfRight)
			{
				while (iteratorLeft != sizeOfLeft)
				{
					merged.push_back(numbers.at(left + iteratorLeft));
					iteratorLeft++;
				}
			}


			indexes.erase(indexes.begin() + i + 1);

		}
		numbers.clear();

		numbers = merged;

		merged.clear();

		myMerge(indexes, merged);


	}
}

void mergeSort()
{
	vector<int> indexes, merged;

	for (int i = 0; i < numbers.size()+1; i++)
	{
		indexes.push_back(i);
	}

	


	// while (!(indexes.empty()))
	// {
	myMerge(indexes, merged);
	// }
	for (int i = 0; i < merged.size(); i++)
	{
		cout << merged.at(i) << endl;
	}

}
};
/*
int main()
{


	//  static const int arrayForInit[] = {8,20,2,15,5,32,18,-4,3};
	static const int arrayForInit[] = { 8, 20, 2, 4, 8, 15, -15, 1, 12, 25, 3, 45 };
	vector<int> numbers(arrayForInit, arrayForInit + sizeof(arrayForInit) / sizeof(arrayForInit[0]));

	mergeSort(numbers);

	return 0;

	*/